package stepDefenition;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class GmailLogin 

{
	
	
	public WebDriver driver=null;
	@Given("^User is on the Home page$")
	public void user_on_home_page()
	{
		
		System.setProperty("webdriver.chrome.driver", "E:\\Software\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.google.co.in");
		driver.findElement(By.linkText("Gmail")).click();
	
		
	}
	
	@And("^User Navigates to the Gmail page$")
	public void user_navigate_to_gmail_page()
	{
		Assert.assertTrue(driver.findElements(By.id("identifierId")).size()==2);
	}
	
	@And("^the User enter valid username and naviagates to the password page$")
	public void user_enter_username(DataTable userCredentials)	
	{
		List<List<String>> data =userCredentials.raw();
		
		driver.findElement(By.id("identifierId")).sendKeys(data.get(0).get(0));
		driver.findElement(By.xpath("//*[@id=\"identifierNext\"]/content/span")).click();
	}	
	
	@And("^the user enter valid password$")
	public void user_enter_valid_password(DataTable userCredentials)
	{
		
		List<List<String>> data =userCredentials.raw();
		//List<Map<String, String>> data =(List<Map<String, String>>) userCredentials.asMap(String.class, String.class);
		//driver.findElement(By.id("identifierId")).sendKeys(data.get(0).get("UserName"));
		driver.findElement(By.name("password")).sendKeys(data.get(0).get(0));
	}	
	
	@When("^User clicks on the login button$")
	public void click_on_login_button()
	{
		driver.findElement(By.xpath("//*[@id=\"passwordNext\"]/content")).click();
	}
	
	@Then("^the user is logged in successfully$")
	public void user_logged_in_successfully()
	{
		System.out.println("User logged in successfully");
	}
	
	
	
	

}
