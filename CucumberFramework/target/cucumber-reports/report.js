$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("E2EFunctionality.feature");
formatter.feature({
  "line": 2,
  "name": "Gmail Feature",
  "description": "I want to test the Gmail feature of thegoogle",
  "id": "gmail-feature",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 5,
  "name": "Login to Gmail successfully",
  "description": "",
  "id": "gmail-feature;login-to-gmail-successfully",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "User is on the Home page",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "User Navigates to the Gmail page",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "the User enter valid username and naviagates to the password page",
  "rows": [
    {
      "cells": [
        "ramkumar9489"
      ],
      "line": 9
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "the user enter valid password",
  "rows": [
    {
      "cells": [
        "S@njana13"
      ],
      "line": 11
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "User clicks on the login button",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "the user is logged in successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "GmailLogin.user_on_home_page()"
});
formatter.result({
  "duration": 20714814126,
  "status": "passed"
});
formatter.match({
  "location": "GmailLogin.user_navigate_to_gmail_page()"
});
formatter.result({
  "duration": 531831841,
  "error_message": "junit.framework.AssertionFailedError\r\n\tat junit.framework.Assert.fail(Assert.java:55)\r\n\tat junit.framework.Assert.assertTrue(Assert.java:22)\r\n\tat junit.framework.Assert.assertTrue(Assert.java:31)\r\n\tat stepDefenition.GmailLogin.user_navigate_to_gmail_page(GmailLogin.java:41)\r\n\tat ✽.And User Navigates to the Gmail page(E2EFunctionality.feature:7)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "GmailLogin.user_enter_username(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "GmailLogin.user_enter_valid_password(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "GmailLogin.click_on_login_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "GmailLogin.user_logged_in_successfully()"
});
formatter.result({
  "status": "skipped"
});
});